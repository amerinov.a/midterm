CREATE TABLE person
(
    id        serial,
    firstname varchar(255),
    lastname  varchar(255),
    city      varchar(255),
    phone     varchar(255),
    telegram  varchar(255)
);
