package kz.advjava.midterm.controller;

import kz.advjava.midterm.entity.Person;
import kz.advjava.midterm.service.PersonService;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class PersonController {
    private final PersonService personService;

    public PersonController(PersonService personService){
        this.personService = personService;
    }

    @GetMapping("/api/v2/person/{id}")
    public ResponseEntity<?> getPerson(@PathVariable Long id){
        return ResponseEntity.ok(personService.getById(id));
    }

    @GetMapping("/api/v2/person")
    public ResponseEntity<?> getPerson(){
        return ResponseEntity.ok(personService.getAll());
    }

    @PostMapping("/api/v2/person")
    public ResponseEntity<?> savePerson(@RequestBody Person person) {
        return ResponseEntity.ok(personService.create(person));
    }

    @PutMapping("/api/v2/person")
    public ResponseEntity<?> updatePerson(@RequestBody Person person) {
        return ResponseEntity.ok(personService.update(person));
    }

    @DeleteMapping("person/{id}")
    public void deletePerson(@PathVariable Long id) {
        personService.delete(id);
    }
}







