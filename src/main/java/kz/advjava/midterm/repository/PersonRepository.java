package kz.advjava.midterm.repository;

import kz.advjava.midterm.entity.Person;
import org.springframework.data.repository.CrudRepository;

public interface PersonRepository extends CrudRepository<Person, Long>{

}
