package kz.advjava.midterm.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table (name = "person")
public class Person {
    @Id
    @Column (name = "id")
    private long id;

    @Column (name = "firstname")
    private String firstname;

    @Column (name = "lastname")
    private String lastname;

    @Column (name = "city")
    private String city;

    @Column (name = "phone")
    private String phone;

    @Column (name = "telegram")
    private String telegram;
}

